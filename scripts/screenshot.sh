#!/bin/bash
directory="$HOME/pictures/screenshots/$(date +"%Y")/$(date +"%m")/$(date +"%d")/"
name="$(date +"%Y%m%d%H%M%S").png"
file="$directory$name"
mkdir -p $directory
maim --hidecursor -m 10 -s -b 3 -c "255,0,0" "$file"

if [ -f "$file" ]; then
    $HOME/scripts/upload.sh "$file"
fi
