#!/bin/bash
case $HOSTNAME in
  "nostromo")
    # hide cursor after 2 seconds of inactivity
    unclutter -idle 2 &

    # disable garbage compositor vsync
    xfwm4 --vblank=off --replace &
    xfconf-query -c xfwm4 -p /general/use_compositing -s true

    # set the wallpaper
    $HOME/scripts/wallpaper.sh &

    # wait for 10 seconds
    sleep 10

    # pulse sink
    sink=`pactl get-default-sink`
    pactl load-module module-null-sink sink_name=Video
    pactl load-module module-null-sink sink_name=Music
    pactl load-module module-remap-source master="$sink" channels=1 master_channel_map=front-left channel_map=mono source_name=Mono source_properties="device.description=Mono"
    pactl set-default-source Mono

    # nextcloud
    flatpak run com.nextcloud.desktopclient.nextcloud --background &

    # wait for 2 seconds
    sleep 2

    # firefox, protonmail
    flatpak run org.mozilla.firefox --new-window "https://mail.proton.me/u/0/inbox" &

    # element
    element-desktop &

    # hexchat
    flatpak run io.github.Hexchat &

    # signal
    flatpak run org.signal.Signal &

    # plexamp
    flatpak run com.plexamp.Plexamp &

    # obsidian
    flatpak run md.obsidian.Obsidian &

    # kitty, btop
    kitty --title btop btop &
    
    # wait for 2 seconds
    sleep 2

    # firefox, nextcloud calendar
    flatpak run org.mozilla.firefox --new-window "https://cloud.cre8r.info/apps/calendar/listMonth/now" &

    # wait for 5 seconds
    sleep 5

    # arrange programs into preset configuration
    # 1 second gaps/repeats to catch stragglers
    $HOME/scripts/arrange.sh &
    sleep 1
    $HOME/scripts/arrange.sh &
    sleep 1
    $HOME/scripts/arrange.sh &
    ;;
  "xana")
    # disable beep
    xset -b

    # hide cursor after 2 seconds of inactivity
    unclutter -idle 2 &

    # disable garbage compositor vsync
    xfwm4 --vblank=off --replace &

    # set the wallpaper
    $HOME/scripts/wallpaper.sh &

    # nextcloud
    flatpak run com.nextcloud.desktopclient.nextcloud --background &
    ;;
esac
