#!/bin/bash

# Desktop
rm -rf $HOME/desktop
rm -rf $HOME/Desktop
ln -s $HOME/Nextcloud/Sync/Desktop $HOME/Desktop
# Documents
rm -rf $HOME/documents
rm -rf $HOME/Documents
ln -s $HOME/Nextcloud/Sync/Documents $HOME/Documents
# Music
rm -rf $HOME/music
rm -rf $HOME/Music
ln -s $HOME/Nextcloud/Sync/Music $HOME/Music
# Pictures
rm -rf $HOME/pictures
rm -rf $HOME/Pictures
ln -s $HOME/Nextcloud/Sync/Pictures $HOME/Pictures
# Videos
rm -rf $HOME/videos
rm -rf $HOME/Videos
ln -s $HOME/Nextcloud/Sync/Videos $HOME/Videos
