#!/bin/bash
if [ -f "$1" ]; then 
  filename=`basename "$1"`
  url=$(sed '1!d' $HOME/.upload)
  vanityurl=$(sed '2!d' $HOME/.upload)
  upload=`curl -s -F filename="$filename" -F image=@"$1" $url`
  echo -n "$vanityurl$upload" | wl-copy
  echo -e "$filename uploaded as $upload\nViewable at: $vanityurl$upload"
  kdialog --title "Screenshot" --passivepopup "$filename uploaded as $upload" 5
else
  echo "invalid file provided"
fi
