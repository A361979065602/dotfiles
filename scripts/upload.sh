#!/bin/bash
if [ -f "$1" ]; then 
  filename=`basename "$1"`
  url=$(sed '1!d' $HOME/.upload)
  vanityurl=$(sed '2!d' $HOME/.upload)
  upload=`curl -s -F filename="$filename" -F image=@"$1" $url`
  echo -n "$vanityurl$upload" | xclip -selection clipboard 1> /dev/null 2> /dev/null
  echo -e "$filename uploaded as $upload\nViewable at: $vanityurl$upload"
  notify-send -i emblem-default "$filename uploaded as $upload"
else
  echo "invalid file provided"
fi
