#!/bin/bash
directory="$HOME/pictures/wallpapers"
wallpaper="$HOME/pictures/wallpapers/18.jpg"

if [[ -z $wallpaper ]]; then
	a=$(find "$directory" -type f | shuf -n 1)
else
	a="$wallpaper"
fi
feh --bg-fill --no-fehbg $a
