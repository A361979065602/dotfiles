#!/bin/bash
case $1 in
  "up")
    wpctl set-volume @DEFAULT_AUDIO_SINK@ 1%+
    ;;
  "down")
    wpctl set-volume @DEFAULT_AUDIO_SINK@ 1%-
    ;;
  "up5")
    wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+
    ;;
  "down5")
    wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-
    ;;
  "mute")
    wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
    ;;
  *)
    wpctl get-volume @DEFAULT_AUDIO_SINK@
    ;;
esac
