#!/bin/bash
case $HOSTNAME in
  "nostromo")
    # hyprland stuff
    dunst &
    dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP &
    systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP &
    gsettings set org.cinnamon.desktop.interface color-scheme 'prefer-dark' &
    gsettings set org.gnome.desktop.interface gtk-theme "Qogir-Dark"   # for GTK3 apps                         
    gsettings set org.gnome.desktop.interface color-scheme "prefer-dark"   # for GTK4 apps
    waybar &
    hypridle &
    hyprpaper &

    # hide cursor after 2 seconds of inactivity
    unclutter -idle 2 &

    # pulse sink
    sink=`pactl get-default-sink`
    pactl load-module module-null-sink sink_name=Video
    pactl load-module module-null-sink sink_name=Music
    pactl load-module module-remap-source master="$sink" channels=1 master_channel_map=front-left channel_map=mono source_name=Mono source_properties="device.description=Mono"
    pactl set-default-source Mono

    # nextcloud
    flatpak run com.nextcloud.desktopclient.nextcloud --background &

    hyprctl dispatch workspace 1
    ;;
esac
