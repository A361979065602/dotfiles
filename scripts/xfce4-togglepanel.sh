#!/bin/bash
status=$(xfconf-query -c xfce4-panel -p /panels/panel-1/autohide-behavior)
if [ $status -eq 0 ]; then
  xfconf-query -c xfce4-panel -p /panels/panel-1/autohide-behavior -t uint -s 2
else
  xfconf-query -c xfce4-panel -p /panels/panel-1/autohide-behavior -t uint -s 0
fi
