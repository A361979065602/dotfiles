#!/bin/bash
case $HOSTNAME in
  "nostromo")
    # protonmail
    wmctrl -r "Mail" -e 0,4826,0,450,478
    wmctrl -r "Mail" -b add,sticky

    # nextcloud calendar
    wmctrl -r "Calendar" -e 0,4374,0,450,478
    wmctrl -r "Calendar" -b add,sticky

    # signal
    wmctrl -r "Signal" -e 0,3840,0,532,478
    wmctrl -r "Signal" -b add,sticky

    # element
    wmctrl -r "Element" -e 0,3840,480,1436,1104
    wmctrl -r "Element" -b add,sticky

    # obsidian
    wmctrl -r "Obsidian" -e 0,3840,2070,634,488
    wmctrl -r "Obsidian" -b add,sticky

    # plexamp
    wmctrl -r "Plexamp" -e 0,5042,1588,239,480
    wmctrl -r "Plexamp" -b add,sticky

    # btop
    wmctrl -r "btop" -e 0,4476,2070,802,486
    wmctrl -r "btop" -b add,sticky

    # hexchat
    wmctrl -r "HexChat" -e 0,3840,1588,1198,478
    wmctrl -r "HexChat" -b add,sticky
    ;;
esac
