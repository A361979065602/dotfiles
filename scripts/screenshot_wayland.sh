#!/bin/bash
directory="$HOME/Pictures/screenshots/$(date +"%Y")/$(date +"%m")/$(date +"%d")/"
name="$(date +"%Y%m%d%H%M%S").png"
file="$directory$name"
mkdir -p $directory
spectacle --background --nonotify --region --output "$file"

if [ -f "$file" ]; then
    $HOME/scripts/upload_wayland.sh "$file"
fi
