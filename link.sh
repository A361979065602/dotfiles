#!/bin/bash
echo "Linking dotfiles..."

# scripts
rm -rf $HOME/scripts
ln -s $HOME/.dotfiles/scripts $HOME/scripts
chmod +x $HOME/scripts/*
echo "READY: $HOME/scripts/"

# bashrc
rm -f $HOME/.bashrc
ln -s $HOME/.dotfiles/.bashrc $HOME/.bashrc
echo "READY: $HOME/.bashrc"

# kitty
rm -rf $HOME/.config/kitty
ln -s $HOME/.dotfiles/kitty $HOME/.config/kitty
echo "READY: $HOME/.config/kitty/"

# mpv
rm -rf $HOME/.config/mpv
ln -s $HOME/.dotfiles/mpv $HOME/.config/mpv
echo "READY: $HOME/.config/mpv/"

# nvim
rm -rf $HOME/.config/nvim
ln -s $HOME/.dotfiles/nvim $HOME/.config/nvim
echo "READY: $HOME/.config/nvim/"

# ranger
rm -rf $HOME/.config/ranger
ln -s $HOME/.dotfiles/ranger $HOME/.config/ranger
chmod +x $HOME/.config/ranger/scope.sh
echo "READY: $HOME/.config/ranger/"

# tmux
rm -rf $HOME/.config/tmux
ln -s $HOME/.dotfiles/tmux $HOME/.config/tmux
echo "READY: $HOME/.config/tmux/"

echo "Linking complete!"
