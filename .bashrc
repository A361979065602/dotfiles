export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export TERM=screen-256color
export VISUAL=nvim
export EDITOR="$VISUAL"

# exit if not running interactively
[[ $- != *i* ]] && return

HISTCONTROL=ignoreboth
shopt -s histappend
HISTSIZE=1000
HISTFILESIZE=2000
shopt -s checkwinsize

# prompt
PS1="$(if [[ ${EUID} == 0 ]]; then echo '\[\033[01;31m\]\u@\h'; else echo '\[\033[01;32m\]\u@\h'; fi)\[\033[33m\] \D{%F %T}\[\033[01;34m\] \w\[\033[00m\]\n\$([[ \$? != 0 ]] && echo \"\")\\$ "

# aliases
## directory listing
alias ls="ls -lA --color"
alias l="ls"
alias w="watch -n 0.1 ls -lsa"

## package management (arch)
updater(){
  Y="\033[1;33m"
  R="\033[0m"
  echo -e "$Y[Beginning ArchLinux updates.]$R"
  echo -e "$Y[Updating flatpaks.]$R"
  sudo flatpak update -y
  echo -e "$Y[Updating arch packages.]$R"
  sudo pacman -Syu --noconfirm
  echo -e "$Y[Clearing arch package cache.]$R"
  sudo pacman -Sc --noconfirm
  echo -e "$Y[Updating AUR packages.]$R"
  sudo aura -Au
  echo -e "$Y[Grabbing latest firmware updates.]$R"
  sudo fwupdmgr refresh
  echo -e "$Y[Performing latest firmware updates.]$R"
  sudo fwupdmgr get-updates
  echo -e "$Y[Regenerate all initramfs images.]$R"
  sudo mkinitcpio -P
  echo -e "$Y[ArchLinux updates complete.]$R"
}
alias u="updater"

## newsboat
alias news="newsboat -q"
alias newsu='newsboat -x print-unread'
alias newsr='newsboat -x reload'
alias n='news'
alias nu='newsu'
alias nr='newsr'

## imaging
alias s="sxiv -a"
alias f="feh"

## clipboard
alias xc="xclip -selection c"

## statistics
alias nf="neofetch --gtk3 off --off"
alias up='uptime -p'

## storage
alias ts='du -ahc | tail -n 1'
alias tss='du -hs * | sort -h'

## terminal/shell
alias cls="clear"
alias c="clear"
alias q="exit"

## text
alias le="less"
alias vim="nvim"
alias v="vim"

## weather
alias weather="curl wttr.in"
radar(){
  wget -q -O /tmp/radar.gif https://radar.weather.gov/ridge/standard/KMKX_loop.gif
  mpv --no-terminal --no-osd-bar --loop /tmp/radar.gif &
}

## media
alias mipc="mpv --input-ipc-server=/home/scipio/.mpvipc"
alias m="mpv --audio-display=no  --ontop --force-window --autofit=380x215"
alias mu="mpv --no-audio-display"
alias youtube-dl="yt-dlp"
alias yta="yt-dlp -f bestaudio[ext=m4a] --embed-thumbnail --add-metadata"
alias mp4dl="yt-dlp -S ext:mp4:m4a"
function mp4conv() {
  filename=$(echo "$1" | cut -d'.' -f1)
  ffmpeg -i "$1" -c:a aac "$filename"_o.mp4
}
function a2v(){
  filename=$(echo "$1" | cut -d'.' -f1)
  ffmpeg -f lavfi -i color=c=black:s=400x150 -i "$1" -c:v libx264 -tune stillimage -pix_fmt yuv420p -shortest -c:a aac -b:a 640k "$filename"_$(date "+%s").mp4
}
alias y='yt-dlp --embed-chapters --embed-subs --embed-thumbnail --embed-chapters -o "%(channel)s - %(title)s.%(ext)s"'

## verbosity
alias cp="cp -v"
alias rm="rm -v"
alias mkdir="mkdir -pv"
alias rmdir="rmdir -v"
alias mv="mv -v"

## misc
alias aid="shuf -i 5000000-9999999 -n 1"
alias sol="ttysolitaire --no-background-color"
alias npm-update="sudo npm install -g npm@latest"
alias flushdns="sudo systemd-resolve --flush-caches"
alias ping="ping -4DUv"
alias upload="$HOME/scripts/upload.sh"
alias be="7z x '*.7z' -o\*"
function r() {
  shuf -n 1 "$1"
}

## internet radio
soma(){
  case $1 in
    "80s")
      echo "Underground 80s"
      mu https://somafm.com/u80s256.pls
      ;;
    "70s")
      echo "Left Coast 70s"
      mu https://somafm.com/seventies320.pls
      ;;
    "fluid")
      echo "Fluid"
      mu https://somafm.com/fluid130.pls
      ;;
    "sonic")
      echo "Sonic Universe"
      mu https://somafm.com/sonicuniverse256.pls
      ;;
    "gs")
      echo "Groove Salad"
      mu https://somafm.com/groovesalad256.pls
      ;;
    "gsc")
      echo "Groove Salad Classic"
      mu https://somafm.com/gsclassic130.pls
      ;;
    "blend")
      echo "Beat Blender"
      mu https://somafm.com/beatblender130.pls
      ;;
    "synph")
      echo "Synphaera Radio"
      mu https://somafm.com/synphaera130.pls
      ;;
    "drone")
      echo "Drone Zone"
      mu https://somafm.com/dronezone130.pls
      ;;
    "trip")
      echo "The Trip"
      mu https://somafm.com/thetrip130.pls
      ;;
    "soul")
      echo "Seven Inch Soul"
      mu https://somafm.com/7soul130.pls
      ;;
    "lush")
      echo "Lush"
      mu https://somafm.com/lush130.pls
      ;;
    "pop")
      echo "PopTron"
      mu https://somafm.com/poptron130.pls
      ;;
    "idm")
      echo "cliqhop idm"
      mu https://somafm.com/cliqhop130.pls
      ;;
    "dub")
      echo "Dub Step Beyond"
      mu https://somafm.com/dubstep130.pls
      ;;
    "dark")
      echo "The Dark Zone"
      mu https://somafm.com/darkzone130.pls
      ;;
    "deep")
      echo "Deep Space One"
      mu https://somafm.com/deepspaceone130.pls
      ;;
    "space")
      echo "Space Station Soma"
      mu https://somafm.com/spacestation130.pls
      ;;
    "n5md")
      echo "n5MD Radio"
      mu https://somafm.com/n5md130.pls
      ;;
    "vapor")
      echo "Vaporwaves"
      mu https://somafm.com/vaporwaves130.pls
      ;;
    "indie")
      echo "Indie Pop Rocks!"
      mu https://somafm.com/indiepop130.pls
      ;;
    *)
      echo "./somafm.sh [station]"
      echo "70s, 80s, blend, dark, deep, drone, dub, fluid, gs, gsc, idm, indie, lush, n5md, pop, sonic, soul, space, synph, trip, vapor"
      ;;
  esac
}
alias npr="mu http://wuwm.streamguys1.com/live.mp3"
