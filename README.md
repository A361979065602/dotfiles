# Dotfiles

## Information
- OS: [Arch Linux](https://archlinux.org/)
- DE: [Xfce](https://www.xfce.org/)
- WM: [Xfwm4](https://docs.xfce.org/xfce/xfwm4/)
- WM Theme: [border-only](https://www.xfce-look.org/p/1016214)
- Theme: [Qogir-dark (GTK2)](https://github.com/vinceliuice/Qogir-theme)
- Icons: elementary (GTK2)
- Terminal: [kitty](https://github.com/kovidgoyal/kitty)
## Software
- [kitty](https://github.com/kovidgoyal/kitty) ([config](/kitty))
- [mpv](https://github.com/mpv-player/mpv) ([config](/mpv))
- [nvim](https://github.com/neovim/neovim) ([config](/nvim))
- [picom](https://github.com/yshui/picom) ([config](/picom.conf))
- [ranger](https://github.com/ranger/ranger) ([config](/ranger))
- [rofi](https://github.com/davatorium/rofi) ([config](/rofi))
- [tmux](https://github.com/tmux/tmux) ([config](/tmux))
## How to use

Go to user's home directory:
```sh
cd ~
```
Clone the repository into a `.dotfiles` directory:
```sh
git clone https://gitlab.com/A361979065602/dotfiles.git .dotfiles
```
Go into the `.dotfiles` directory:
```sh
cd .dotfiles
```
Mark the `link.sh` script as executable:
```sh
chmod +x link.sh
```
Run the `link.sh` script:
> Make sure to review the script before running. It will attempt to make several symbolic links, deleting any existing data that might be in the way of the script completing successfully.
```sh
./link.sh
```
